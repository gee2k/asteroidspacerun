﻿ #pragma once
#include <glm/glm.hpp>
#include "collisionHandling.h"

class movingObject : public collisionHandling
{
public:
	movingObject::movingObject(glm::vec3 position, glm::vec3 direction, float stepSize, float scaleFactor, glm::vec3 rotationSpeedRandom);


	//for collision detection
	glm::vec3 getPosition() const;

	//for collision detection
	void setPosition(glm::vec3 objPosition);

	//speed for rotation
	glm::vec3 getRotationSpeed() const;

	//return the values of rotation on their axis
	glm::vec3 getRotationAngle() const;

	//rotationAxis taken for rotation
//	glm::vec3 getRotationAxis() const;



	//movement
	void calculateNewPosition(float boost);
	
	//
	bool detectCollision(movingObject* object) override;

	// update rotation angles so asteroid rotates around all three axes
	void updateRotationAngles(float boost);

	float getScale() const;

protected:
	glm::vec3 position;		//position in world space
	glm::vec3 direction;	//direction asteroid moves
	glm::vec3 rotationSpeed;	//speed of the different axis
	glm::vec3 rotationDegrees;	//value the Objects rotation axis have
	//axis not needed, achses are implicitly declared when pos.x times speed.x ... it mybe that wie calculate for the x axis ;)
	//glm::vec3 rotationAxis;			//axis which it rotates around
	float stepSize = 0;
	float scaleFactor = 0;
};

