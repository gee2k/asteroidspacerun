#include "explosion.h"
#include <glm/gtc/matrix_transform.inl>


explosion::explosion(glm::vec3 position, int ttl):m_position(position), m_ttl(ttl){}


bool explosion::canBeDeleted() const{
	
	return m_deleteMe;
}

float explosion::getTTL() const
{
	return m_ttl;
}

void explosion::calculateTimespan(float slowDown, float decrease, float deleteMeThreshold)
{
	m_ttl = m_ttl - decrease * slowDown;
	if (m_ttl < deleteMeThreshold)
	{
		m_deleteMe = true;
	}
}

glm::vec3 explosion::getPosition() const
{
	return m_position;
}

explosion::~explosion()
{
}
