﻿#include "movingObject.h"
#include <iostream>

movingObject::movingObject(glm::vec3 position, glm::vec3 direction, float stepSize, float scaleFactor, glm::vec3 rotationSpeedRandom) :position(position), direction(direction), stepSize(stepSize), scaleFactor(scaleFactor), rotationSpeed(rotationSpeedRandom)
{
	setCollisionRadiusXpos(1 * scaleFactor);
	setCollisionRadiusXneg(1 * scaleFactor);
	setCollisionRadiusYpos(1.2 * scaleFactor);
	setCollisionRadiusYneg(1.2 * scaleFactor);
	setCollisionRadiusZpos(1 * scaleFactor);
	setCollisionRadiusZneg(1.8 * scaleFactor);
}

//for collision detection
glm::vec3 movingObject::getPosition() const
{
	return position;
}

void movingObject::setPosition(glm::vec3 objPosition)
{
	position = objPosition;
}

//speed for rotation
glm::vec3 movingObject::getRotationSpeed() const
{
	return rotationSpeed;
}

//return the values of rotation on their axis
glm::vec3 movingObject::getRotationAngle() const
{
	return rotationDegrees;
}

float movingObject::getScale() const {
	return scaleFactor;
}

void movingObject::calculateNewPosition(float boost) {
	position = position - (direction * glm::vec3(0, 0, stepSize*boost));
}

bool movingObject::detectCollision(movingObject* object) { return false; }

	void movingObject::updateRotationAngles(float boost)
	{
		//rotationAngleX = (int)(rotationAngleX * rotationSpeedX * 1) % 360 + 1;

		rotationDegrees.x = fmod((rotationDegrees.x + rotationSpeed.x * boost), 360 + 1);
		rotationDegrees.y = fmod((rotationDegrees.y + rotationSpeed.y * boost), 360 + 1);
		rotationDegrees.z = fmod((rotationDegrees.z + rotationSpeed.z * boost), 360 + 1);


}
