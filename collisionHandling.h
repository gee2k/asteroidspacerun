﻿#pragma once


class movingObject;	//foreward declaration

class collisionHandling
{
public:
	virtual bool detectCollision(movingObject* object) = 0;

	virtual void collisionReaction() = 0;

	bool getCollisionDetected() const
	{
		return collisionDetected;
	}

	void setCollisionDetected(bool boolean)
	{
		collisionDetected = boolean;
	}

	float getCollisionRadiusXpos() const
	{
		return collisionRadiusXpos;
	}

	float getCollisionRadiusXneg() const
	{
		return collisionRadiusXneg;
	}

	float getCollisionRadiusYpos() const
	{
		return collisionRadiusYpos;
	}

	float getCollisionRadiusYneg() const
	{
		return collisionRadiusYneg;
	}

	float getCollisionRadiusZpos() const
	{
		return collisionRadiusZpos;
	}

	float getCollisionRadiusZneg() const
	{
		return collisionRadiusZneg;
	}

	void setCollisionRadiusXpos(float radius)
	{
		collisionRadiusXpos = radius;
	}

	void setCollisionRadiusXneg(float radius)
	{
		collisionRadiusXneg = radius;
	}

	void setCollisionRadiusYpos(float radius)
	{
		collisionRadiusYpos = radius;
	}

	void setCollisionRadiusYneg(float radius)
	{
		collisionRadiusYneg = radius;
	}

	void setCollisionRadiusZpos(float radius)
	{
		collisionRadiusZpos = radius;
	}

	void setCollisionRadiusZneg(float radius)
	{
		collisionRadiusZneg = radius;
	}

	

	virtual ~collisionHandling() {}

protected:
	bool collisionDetected = false;
	float collisionRadiusXpos;
	float collisionRadiusXneg;
	float collisionRadiusYpos;
	float collisionRadiusYneg;
	float collisionRadiusZpos;
	float collisionRadiusZneg;



};
