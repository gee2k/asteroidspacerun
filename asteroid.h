#pragma once
#include "movingObject.h"

class asteroid : public movingObject
{
public:
	using movingObject::movingObject;	//constructor inheritance

	void collisionReaction() override;
};

