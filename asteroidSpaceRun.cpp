// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

// Include GLEW
#include <GL/glew.h> 

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ctime>
#include "fuel.h"
#include "explosion.h"
using namespace glm;

// Achtung, die OpenGL-Tutorials nutzen glfw 2.7, glfw kommt mit einem ver�nderten API schon in der Version 3 
 
// Befindet sich bei den OpenGL-Tutorials unter "common"
#include "shader.hpp"

//teapot includen
#include "objloader.hpp"

//texture loader includen
#include "texture.hpp"

//own files
#include "asteroid.h"
#include "wall.h"
#include "ship.h"

void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

//global stuff
//gameTech:

int screenWidth = 1024;
int screenHeight = 768;
const char* gameTitle = "CG - Tutorial";

float farPlane = 200.0f;

glm::mat4 MVP;

//timeSmoothing
double lastTime = glfwGetTime();
float deltaTime = 0;
double currentTime = glfwGetTime();
float inputSpeedBias = 30.0f;

bool displayDeltaOrFPS = false;
//fps measuring

int fps = 0;
int fpsOut = 0;
int lastSecond = 0;

//ingameStuff:
bool collisions = true;
unsigned int amountOfMovingObjectsInGame = 100;	// amount of asteroids in game, can be increased during game to increase difficulty
//std::vector<asteroid*> asteroids;	//list containing all the asteroids
std::vector<movingObject*> movingObjects;	//list containing all moving objects
std::vector<explosion*> explosions; //list containing all the asteroids
float scaleFactor = 2.0f; // for everything !

//bar calculation values between 0.53 and 1.0
float barDynamicRange = 1.0f - 0.53f;
float gameDifficulty = 0.0f;
int actualDifficulty = 1;	//increased till max Diff
int maxDifficulty = 5;		//bla bla
int difficultyIncreaseTime = 30;
float fuelLeft = 1.0f;
float damageTaken = 0.0f;
float damageDecrease = 8.0f;	//compensated by , less difficult ;)

float fuelChargeAmount = 0.3f;	//amount recharged trough refueling
float fuelDininishFactor = 0.0002f;	//amount decrease per deltaTime;	(1 us way to easy and 3 ist to fast. 2 is almost rechargable ingame, almost ;)
bool canSteer = true;
bool gameLost = false;
float speedModifier = 1.0f;	//used for acceleration and slowdown of movement

// asteroids details
int movingObjectCreationCoordinateX = 0; // random x coordinate between 1 and 50
int movingObjectCreationCoordinateY = 0; // random y coordinate between 1 and 50
int depthWhereMovingObjectsAreCreated = 50.0f; // random z coordinate between 50 and 100
glm::vec3 astDirection = glm::vec3(0, 0, 1);
float astVelocity = 0.4f;		//asteroids speed

// environmentDetails
float envSpeed = 0.8f;				//speed of the Environment
unsigned int numberOfWalls;
float fuelSpeed = 0.11f;			//speed of the fuels

// Ship details
float moveShipX = 0.0f;	// Variable for moving spaceship horizontally
float ShipXAmountMoving = 0.001f; // 0.1f on old system movement step size on x axis
float moveShipY = 0.0f;	// Variable for moving spaceship vertically
float ShipYAmountMoving = 0.001f; // // movement step size y axis
ship* spaceship = new ship(glm::vec3(moveShipX, moveShipY, 0), 1.0f);

float shipNose = 0.0f;
float shipFlaps = 0.0f;

bool compensateVert = true;		//activates the compensation of the rotation trough button presses
bool compensateHZ = true;		//activates the compensation of the rotation trough button presses
bool buttonPressedHZ = false;	//to activate the rotation compensation if no button is pressed
bool buttonPressedVert = false;	//to activate the rotation compensation if no button is pressed

//pseudo rand DEBUG!
//int changePosBla = 0;

//Asteroiden Global stuff
bool astBuilt = false;
std::vector<glm::vec3> astVerts;
std::vector<glm::vec2> astUVs;
std::vector<glm::vec3> astNormals;

GLuint asteroidVertsArray = 0;
GLuint asteroidVertexBuffer = 0;
GLuint asteroidNormalBuffer = 0;
GLuint asteroidUVBuffer = 0;
const char* astObj = "myasteroid_03_high.obj";

const char* astTexPath = "astro_texture1.bmp";
GLuint astTex = 0;

//Fuel Global stuff
bool fuelBuilt = false;
std::vector<glm::vec3> fuelVerts;
std::vector<glm::vec2> fuelUVs;
std::vector<glm::vec3> fuelNormals;

GLuint fuelVertsArray = 0;
GLuint fuelVertexBuffer = 0;
GLuint fuelNormalBuffer = 0;
GLuint fuelUVBuffer = 0;
const char* fuelObj = "kanister.obj";

const char* fuelTexPath = "kanister.bmp";
GLuint fuelTex = 0;

int fuelTime = 500;	//first spawn time after a fuel is beeing created, decreased over time - updated then with fuelTimeInterval
int fuelTimeInterval = 1000;
bool createFuelMovingObject = false;

//Ship Global stuff
bool shipBuilt = false;
std::vector<glm::vec3> shipVerts;
std::vector<glm::vec2> shipUVs;
std::vector<glm::vec3> shipNormals;

GLuint shipVertsArray = 0;
GLuint shipVertexBuffer = 0;
GLuint shipNormalBuffer = 0;
GLuint shipUVBuffer = 0;
const char* shipObj = "spaceship_2b.obj";

const char* shipTexPath = "spaceship.bmp";
GLuint shipTex = 0;

glm::mat4 shipPosition = glm::mat4(1.0f);	//initial ship position

//Space Environment
bool spaceEnvBuilt = false;
std::vector<glm::vec3> spaceEnvVerts;
std::vector<glm::vec2> spaceEnvUVs;
std::vector<glm::vec3> spaceEnvNormals;

GLuint spaceEnvVertsArray = 0;
GLuint spaceEnvVertexBuffer = 0;
GLuint spaceEnvNormalBuffer = 0;
GLuint spaceEnvUVBuffer = 0;
const char* spaceEnvObj = "spaceSphere.obj";

const char* spaceEnvTexPath = "spaceEnvironment_B.bmp";
GLuint spaceEnvTex = 0;

//starfield - uses simplePolygon
GLuint wallTex = 0;
int wallScale = 5;
int drawScale = 0;	//calculated final drawScale depends on wallScale wich is modified used for star cascade

int cascadeDistance = 5;

int maxCascade = 2;	//to depth layers of stars
int wallBuildDepth = 4;
const char* wallTexPath = "starWall.bmp";

std::vector<glm::vec3> wallPos;

//shader stuff
GLint loc;	//variable which holds access to the mvUV var declared there to move the uvs
GLint loc2;	//variable which holds access to the fragmentshader transparency var
GLint loc3;	//variable which holds access to the fragmentshader transparency var

GLfloat mvuv = 0.0f;
GLfloat mvuviter = 0.1f/20;	//speed the walls are animated

GLfloat wallTransparency = 0.0f;	//value to change the transparency value of the wall

float x = 0.5f;
float y = 0.5f;
int width = 100;;
int height = 100;;

float max_value = 1.0f;
float progress = 0.0f;
float damageProgress = 0.5f;

bool billboardCreated = false;
GLuint billboardProgramID;	//shader program
GLuint billboardVertexArrayID;
GLuint billboard_vertex_buffer;

// Vertex shader
GLuint billboardCameraRight_worldspace_ID;
GLuint billboardCameraUp_worldspace_ID;
GLuint billboardViewProjMatrixID;
GLuint billboardPosID;
GLuint billboardSizeID;
GLuint billboardLifeLevelID;
GLuint billboardTextureID;

GLuint billboardMyColorID;
GLuint billboardVertexOffsetID;

glm::mat4 billboardViewProjectionMatrix;

GLuint billboardTexture;

int maxUIElements = 3;
float red, green, blue, alpha;
float billboardMyColor[4] = { red = 0.5f, green = 0.5f, blue = 0.5f, alpha = 1.0f };	//basis animated bar color

GLuint billboardDifficultyTexture;
glm::vec3 billboardDifficultyPosition = glm::vec3(0.0f, -2.0f, 0.0f);
glm::vec2 billboardDifficultyScale = glm::vec2(2.0f, 0.2f);
const char* billboardDifficultyTexturePath = "difficultyBar.DDS";

GLuint billboardFuelTexture;
glm::vec3 billboardFuelPosition = glm::vec3(-2.0f, 1.5f, 0.0f);
glm::vec2 billboardFuelScale = glm::vec2(1.024f, 0.25f);
const char* billboardFuelTexturePath = "FuelBar.DDS";

GLuint billboardDamageTexture;
glm::vec3 billboardDamagePosition = glm::vec3(2.0f, 1.5f, 0.0f);
glm::vec2 billboardDamageScale = glm::vec2(1.024f, 0.25f);
const char* billboardDamageTexturePath = "DamageBar.DDS";

float billboardLifeLevel = 0.0f;

// The VBO containing the 4 vertices of the particles.
const GLfloat billboardg_vertex_buffer_data[] = {
	-0.5f, -0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	-0.5f,  0.5f, 0.0f,
	0.5f,  0.5f, 0.0f,
};

//simplePoly Stuff from here
bool simplePolyBuilt = false;
std::vector<glm::vec3> simplePolyVerts;
std::vector<glm::vec2> simplePolyUVs;
std::vector<glm::vec3> simplePolyNormals;

GLuint simplePolyVertsArray = 0;
GLuint simplePolyVertexBuffer = 0;
GLuint simplePolyNormalBuffer = 0;
GLuint simplePolyUVBuffer = 0;
const char* simplePolyObj = "simplePoly.obj";

const char* simplePolyTexPath = "mandrill.bmp";
GLuint simplePolyTex = 0;

//additional textures used by simplePoly

//wall/starfield stuff above

//explosion stuff
GLuint explosionTex = 0;
const char* explosionTexPath = "expl.bmp";
bool testDrawExplosion = false;
int explosionTTL = 100;

// Diese Drei Matrizen global (Singleton-Muster), damit sie jederzeit modifiziert und
// an die Grafikkarte geschickt werden koennen
glm::mat4 Projection;
glm::mat4 View;
glm::mat4 Model;
GLuint programID;
GLuint programIDanim;
GLuint programIDExpl;

//Camera Stuff
float angleX = 40.0f;		//intro value
float angleY = 40.0f;		//intro value
float angleZ = 40.0f;		//intro value

float maxX = 20.0f;			//outro value
float maxY = 80.0f;			//outro value
float maxZ = 10.0f;			//outro value

//check if end animation ended
bool endXReached = false;	
bool endYReached = false;
bool endZReached = false;

bool resetCam = true;		//true for intro animation ;)
float camResetBoost = 2.0f;	//set to 1 if no boost is needed. NOT NULL !

bool cameraShake = false;
float amountCameraShake = 2.0f;	//aktual taken shake amount
int cameraShakeTime = 0;

float bigShakeAmount = 2.0f;		//amount for big Explosions
int bigShakeTime = 50;		//time big shake happens

float fovReduce = 0.0f;
float fovReduceBoostAdditive = 90.0f;

const float MIN_RAND = 0.0, MAX_RAND = 1.0;
const float range = MAX_RAND - MIN_RAND;

bool cam1 = true;	//total behind cam as standart
bool cam2 = false;
bool cam3 = false;


void switchCam(int camera)
{
	switch (camera)
	{
	case 1:
		cam1 = true;
		cam2 = false;
		cam3 = false;
		break;
	case 2:
		cam1 = false;
		cam2 = true;
		cam3 = false;
		break;
	case 3:
		cam1 = false;
		cam2 = false;
		cam3 = true;
	}
}

//key bindings here
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	switch (key)
	{
	case GLFW_KEY_ESCAPE:
		glfwSetWindowShouldClose(window, GL_TRUE);
		break;
/*	case GLFW_KEY_X:
		//achseX = glm::vec3(1, 0, 0);
		angleX += 2.0f;
		break;
	case GLFW_KEY_Y:
		//achseY = glm::vec3(0, 1, 0);
		angleY += 2.0f;
		break;
	case GLFW_KEY_Z:
		//achseZ = glm::vec3(0, 0, 1);
		angleZ += 2.0f;
		break;
	case GLFW_KEY_F:
		displayDeltaOrFPS = !displayDeltaOrFPS;
		break;

	case GLFW_KEY_COMMA:
		++damageProgress;
		break;

	case GLFW_KEY_D:
		damageTaken = 1.0f;
		break;*/

	case GLFW_KEY_1:
		switchCam(1);
		break;

	case GLFW_KEY_2:
		switchCam(2);
		break;

	case GLFW_KEY_3:
		switchCam(3);
		break;

		//reset Cam
/*	case GLFW_KEY_R:
		resetCam = true;
		break;*/

	default:
		break;
	}
}

void keyInput(GLFWwindow* window)
{

	buttonPressedHZ = false;
	buttonPressedVert = false;
	
		//keyboard
		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && canSteer) {
			buttonPressedVert = true;
			//spaceShipUp
			if (moveShipY <= 2)
				moveShipY += ShipYAmountMoving * deltaTime * inputSpeedBias;

			if (shipNose > -15.0f)
				shipNose = shipNose - deltaTime;

			compensateVert = false;

		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && canSteer) {
			buttonPressedVert = true;
			//spaceShipDown
			if (moveShipY >= -2)
				moveShipY -= ShipYAmountMoving * deltaTime * inputSpeedBias;
			if (shipNose < 15.0f)
				shipNose = shipNose + deltaTime;

			compensateVert = false;

		}
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && canSteer) {
			buttonPressedHZ = true;
			//spaceShipLeft
			if (moveShipX <= 2)
				moveShipX += ShipXAmountMoving * deltaTime * inputSpeedBias;

			if (shipFlaps > -70.0f)
				if (shipFlaps > 0.0f)
					shipFlaps = shipFlaps - (5.0f * deltaTime);
				else
					shipFlaps = shipFlaps - (3.0f * deltaTime);

			compensateHZ = false;
		}
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && canSteer) {
			buttonPressedHZ = true;
			//spaceShipRIght
			if (moveShipX >= -2)
				moveShipX -= ShipXAmountMoving * deltaTime * inputSpeedBias;

			if (shipFlaps < 70.0f)
				if (shipFlaps < 0.0f)
					shipFlaps = shipFlaps + (5.0f * deltaTime);
				else
					shipFlaps = shipFlaps + (3.0f * deltaTime);

			compensateHZ = false;		
	}
	
	//no button ? compensate rotation !
	if (!buttonPressedVert)
	{
		compensateVert = true;
	}

	if (!buttonPressedHZ)
	{
		compensateHZ = true;
	}
}

void resetCamToOrg(bool endAnimation)
{

	if (!endAnimation) {
		if ((angleX > 0.1) && (angleX < 180))
			angleX -= 0.1f * deltaTime / 2 * camResetBoost;
		else if ((angleX < -0.1) && (angleX >= -180))
			angleX += 0.1f * deltaTime / 2 * camResetBoost;
		else
			angleX = 0.0f;

		if ((angleY > 0.1) && (angleY < 180))
			angleY -= 0.1f * deltaTime / 2 * camResetBoost;
		else if ((angleY < -0.1) && (angleY >= -180))
			angleY += 0.1f * deltaTime / 2 * camResetBoost;
		else
			angleY = 0.0f * deltaTime / 2 * camResetBoost;
		
		if ((angleZ > 0.1) && (angleZ < 180))
			angleZ -= 0.1f * deltaTime / 2 * camResetBoost;
		else if ((angleZ < -0.1) && (angleZ >= -180))
			angleZ += 0.1f * deltaTime / 2 * camResetBoost;
		else
			angleZ = 0.0f;

	}
	else if (endAnimation){


		if (angleX < maxX - 0.1f)
			angleX += 0.1f;// *deltaTime / 2 * camResetBoost;
		else if (angleX > maxX + 0.1f)
			angleX -= 0.1f;// *deltaTime / 2 * camResetBoost;
		else {
			angleX = maxX;
			endXReached = true;
		}

		if (angleY < maxY - 0.1f)
			angleY += 0.2f;// *deltaTime / 2 * camResetBoost;
		else if (angleY > maxY + 0.1f)
			angleY -= 0.2f;// *deltaTime / 2 * camResetBoost;
		else {
			angleY = maxY;
			endYReached = true;
		}

		if (angleZ < maxZ - 0.1f)
			angleZ += 0.2f;// *deltaTime / 2 * camResetBoost;
		else if (angleZ > maxZ + 0.1f)
			angleZ -= 0.2f;// *deltaTime / 2 * camResetBoost;
		else {
			angleZ = maxZ;
			endZReached = true;
		}
	}

	if ((angleX == 0) && (angleY == 0) && (angleZ == 0))
	{
		resetCam = false;
	}
}

void sendMVP()
{
	// Our ModelViewProjection : multiplication of our 3 matrices
	//glm::mat4 MVP = Projection * View * Model; 
	MVP = Projection * View * Model;
	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform, konstant fuer alle Eckpunkte
	glUniformMatrix4fv(glGetUniformLocation(programID, "MVP"), 1, GL_FALSE, &MVP[0][0]);

	//neu
	glUniformMatrix4fv(glGetUniformLocation(programID, "M"), 1, GL_FALSE, &Model[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(programID, "V"), 1, GL_FALSE, &View[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(programID, "P"), 1, GL_FALSE, &Projection[0][0]);
}

/*
* erzeugt den grafik stuff f�r den Asteroiden, muss nur einmal durchlaufen
*/
void createAsteroid() {

		bool res = loadOBJ(astObj, astVerts, astUVs, astNormals);
		glGenVertexArrays(1, &asteroidVertsArray);	//vertex array beinhalten vertexbuffers (VBO) (glGenBUffer)
		glBindVertexArray(asteroidVertsArray);

		glGenBuffers(1, &asteroidVertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, asteroidVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, astVerts.size() * sizeof(glm::vec3), &astVerts[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glGenBuffers(1, &asteroidNormalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, asteroidNormalBuffer);
		glBufferData(GL_ARRAY_BUFFER, astNormals.size() * sizeof(glm::vec3), &astNormals[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glGenBuffers(1, &asteroidUVBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, asteroidUVBuffer);
		glBufferData(GL_ARRAY_BUFFER, astUVs.size() * sizeof(glm::vec2), &astUVs[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

		astTex = loadBMP_custom(astTexPath);

		astBuilt = true;
		std::cout << "asteroid gfx built" << std::endl;
}

void createBillboard()
{
	//GLuint VertexArrayID5;
	glGenVertexArrays(1, &billboardVertexArrayID);
	glBindVertexArray(billboardVertexArrayID);


	// Create and compile our GLSL program from the shaders
	billboardProgramID = LoadShaders("Billboard.vertexshader", "Billboard.fragmentshader");

	// Vertex shader
	billboardCameraRight_worldspace_ID = glGetUniformLocation(billboardProgramID, "CameraRight_worldspace");
	billboardCameraUp_worldspace_ID = glGetUniformLocation(billboardProgramID, "CameraUp_worldspace");
	billboardViewProjMatrixID = glGetUniformLocation(billboardProgramID, "VP");
	billboardPosID = glGetUniformLocation(billboardProgramID, "BillboardPos");
	billboardSizeID = glGetUniformLocation(billboardProgramID, "BillboardSize");
	billboardLifeLevelID = glGetUniformLocation(billboardProgramID, "LifeLevel");
	billboardMyColorID = glGetUniformLocation(billboardProgramID, "myColor");
	billboardTextureID = glGetUniformLocation(billboardProgramID, "myTextureSampler");
	billboardVertexOffsetID = glGetUniformLocation(billboardProgramID, "BillboardShaderSize");
	//load all the other ui elements
	billboardDifficultyTexture = loadDDS(billboardDifficultyTexturePath);
	billboardFuelTexture = loadDDS(billboardFuelTexturePath);
	billboardDamageTexture = loadDDS(billboardDamageTexturePath);
	
	glGenBuffers(1, &billboard_vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, billboard_vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(billboardg_vertex_buffer_data), billboardg_vertex_buffer_data, GL_DYNAMIC_DRAW);


	billboardCreated = true;
	std::cout << "billboard created" << std::endl;
}

void createFuel() {

	bool res = loadOBJ(fuelObj, fuelVerts, fuelUVs, fuelNormals);
	glGenVertexArrays(1, &fuelVertsArray);	//vertex array beinhalten vertexbuffers (VBO) (glGenBUffer)
	glBindVertexArray(fuelVertsArray);

	glGenBuffers(1, &fuelVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, fuelVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, fuelVerts.size() * sizeof(glm::vec3), &fuelVerts[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &fuelNormalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, fuelNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, fuelNormals.size() * sizeof(glm::vec3), &fuelNormals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &fuelUVBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, fuelUVBuffer);
	glBufferData(GL_ARRAY_BUFFER, fuelUVs.size() * sizeof(glm::vec2), &fuelUVs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	fuelTex = loadBMP_custom(fuelTexPath);

	fuelBuilt = true;
	std::cout << "fuel gfx built" << std::endl;
}

void createShip() {

	bool res = loadOBJ(shipObj, shipVerts, shipUVs, shipNormals);
	glGenVertexArrays(1, &shipVertsArray);	//vertex array beinhalten vertexbuffers (VBO) (glGenBUffer)
	glBindVertexArray(shipVertsArray);

	glGenBuffers(1, &shipVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, shipVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, shipVerts.size() * sizeof(glm::vec3), &shipVerts[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &shipNormalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, shipNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, shipNormals.size() * sizeof(glm::vec3), &shipNormals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &shipUVBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, shipUVBuffer);
	glBufferData(GL_ARRAY_BUFFER, shipUVs.size() * sizeof(glm::vec2), &shipUVs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	shipTex = loadBMP_custom(shipTexPath);

	shipBuilt = true;
	std::cout << "ship gfx built" << std::endl;
}


void createEnvironment() {

	bool res = loadOBJ(spaceEnvObj, spaceEnvVerts, spaceEnvUVs, spaceEnvNormals);
	glGenVertexArrays(1, &spaceEnvVertsArray);	//vertex array beinhalten vertexbuffers (VBO) (glGenBUffer)
	glBindVertexArray(spaceEnvVertsArray);

	glGenBuffers(1, &spaceEnvVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, spaceEnvVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, spaceEnvVerts.size() * sizeof(glm::vec3), &spaceEnvVerts[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &spaceEnvNormalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, spaceEnvNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, spaceEnvNormals.size() * sizeof(glm::vec3), &spaceEnvNormals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &spaceEnvUVBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, spaceEnvUVBuffer);
	glBufferData(GL_ARRAY_BUFFER, spaceEnvUVs.size() * sizeof(glm::vec2), &spaceEnvUVs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	spaceEnvTex = loadBMP_custom(spaceEnvTexPath);

	spaceEnvBuilt = true;
	std::cout << "spaceEnv gfx built" << std::endl;
}

void createSimplePolygon() {

	bool res = loadOBJ(simplePolyObj, simplePolyVerts, simplePolyUVs, simplePolyNormals);
	glGenVertexArrays(1, &simplePolyVertsArray);	//vertex array beinhalten vertexbuffers (VBO) (glGenBUffer)
	glBindVertexArray(simplePolyVertsArray);

	glGenBuffers(1, &simplePolyVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, simplePolyVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, simplePolyVerts.size() * sizeof(glm::vec3), &simplePolyVerts[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &simplePolyNormalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, simplePolyNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, simplePolyNormals.size() * sizeof(glm::vec3), &simplePolyNormals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &simplePolyUVBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, simplePolyUVBuffer);
	glBufferData(GL_ARRAY_BUFFER, simplePolyUVs.size() * sizeof(glm::vec2), &simplePolyUVs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	simplePolyTex = loadBMP_custom(simplePolyTexPath);
	
	//starfield Tex
	wallTex = loadBMP_custom(wallTexPath);

	//explosion tex
	explosionTex = loadBMP_custom(explosionTexPath);

	simplePolyBuilt = true;
	std::cout << "simplePoly gfx built" << std::endl;
}

/*
1. translate
2. scale.
3. draw !
*/

/*
* Zeichnet einen Asteroiden
* @param newTranslation -> die Position im Raum wo er erscheinen soll
*/
void drawAsteroid(glm::mat4 newTranslation, float scale) {

	if (!astBuilt) {
		createAsteroid();
	}
	glUseProgram(programID);

	//save Model
	glm::mat4 Save = Model;

	//3. translaten
	Model = newTranslation;

	//2. scale do
	Model = glm::scale(Model, glm::vec3(scale, scale, scale));

	sendMVP();
	//1. Draw

	//texturing
	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, astTex);

	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);


	//final drawing !
	glBindVertexArray(asteroidVertsArray);

	glDrawArrays(GL_TRIANGLES, 0, astVerts.size());
	glBindVertexArray(0);

	//reset Model
	Model = Save;
}

/*
* Zeichnet ein Benzinkanister
* @param newTranslation -> die Position im Raum wo er erscheinen soll
*/
void drawFuel(glm::mat4 newTranslation) {

	if (!fuelBuilt) {
		createFuel();
	}
	glUseProgram(programID);

	//save Model
	glm::mat4 Save = Model;

	//3. translaten
	Model = newTranslation;

	//2. scale 

	sendMVP();
	//1. Draw

	//texturing
	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fuelTex);

	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);


	//final drawing !
	glBindVertexArray(fuelVertsArray);

	glDrawArrays(GL_TRIANGLES, 0, fuelVerts.size());
	glBindVertexArray(0);

	//reset Model
	Model = Save;
}

/*
* Zeichnet das Raumschiff und versetzt es entsprechend seiner positionen
* @param newTranslation -> die Position im Raum wo er erscheinen soll
*/
void drawShip() {

	if (!gameLost) {
		if (!shipBuilt) {
			createShip();
		}
		glUseProgram(programID);

		//save Model
		glm::mat4 Save = Model;

		//3. translaten
		//ship rotation fix
		Model = glm::rotate(Model, 180.0f, glm::vec3(0, 0, 1));


		// input calculation for ship
		Model = glm::translate(Model, glm::vec3(moveShipX, moveShipY, 0));


		//rotation from here
		Model = glm::rotate(Model, shipNose, glm::vec3(1, 0, 0));
		Model = glm::rotate(Model, shipFlaps, glm::vec3(0, 0, 1));


		//2. scale
		Model = glm::scale(Model, glm::vec3(1.0 / 2.0, 1.0 / 2.0, 1.0 / 2.0));

		sendMVP();
		//1. Draw

		//texturing
		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, shipTex);

		// Set our "myTextureSampler" sampler to user Texture Unit 0
		glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);


		//final drawing !
		glBindVertexArray(shipVertsArray);

		glDrawArrays(GL_TRIANGLES, 0, shipVerts.size());
		glBindVertexArray(0);

		//reset Model
		Model = Save;
	}
}

/*
* Zeichnet das Raumschiff und versetzt es entsprechend seiner positionen
* @param newTranslation -> die Position im Raum wo er erscheinen soll
*/
void drawEnvironment() {

	if (!spaceEnvBuilt) {
		createEnvironment();
	}

	//save Model
	glm::mat4 Save = Model;
	glUseProgram(programID);

	//3. translaten
	Model = glm::rotate(Model, 180.0f, glm::vec3(1, 0, 0));
	// input calculation for ship
	Model = glm::translate(Model, glm::vec3(0, 0, 0));


	//2. scale
	Model = glm::scale(Model, glm::vec3(100.0, 100.0, 100.0));

	sendMVP();
	//1. Draw

	//texturing
	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, spaceEnvTex);

	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);


	//final drawing !
	glBindVertexArray(spaceEnvVertsArray);

	glDrawArrays(GL_TRIANGLES, 0, spaceEnvVerts.size());
	glBindVertexArray(0);

	//reset Model
	Model = Save;
}

void drawWall() {

	if (!simplePolyBuilt) {
		createSimplePolygon();
	}
	

	glm::mat4 Save = Model;
	glUseProgram(programIDanim);


	//uv modifcation
	if (loc != -1)
	{
		//uv between 0 and 1
		if (mvuv >= 1)
			mvuv -= 1;

		//increase mvuv
		mvuv += mvuviter * speedModifier;
		glUniform1f(loc, mvuv);
	}

	//cascading

	for (int k = 0; k < maxCascade; ++k)
	{
		glUseProgram(programIDanim);	//access to shader
		int drawWallBuildDepth = 0;

		if (k < 1) {
			drawScale = wallScale;
			drawWallBuildDepth = wallBuildDepth;
			//transparency modifications
			if (loc2 != -1)
			{
				wallTransparency = 0.8;
			}
		}
		else{
			drawScale = 10* (cascadeDistance * k);
			drawWallBuildDepth = 10* wallBuildDepth;
			//transparency modifications
			if (loc2 != -1)
			{
				wallTransparency = 0.9;
			}
		}

		glUniform1f(loc2, wallTransparency);	//access to shader
		wallPos = { glm::vec3(-drawScale, 0, drawWallBuildDepth),glm::vec3(0, drawScale, drawWallBuildDepth),glm::vec3(drawScale, 0, drawWallBuildDepth),glm::vec3(0, -drawScale, drawWallBuildDepth) };


		for (int i = 0; i < 4; ++i)
		{
			glUseProgram(programIDanim);


			//save Model
			Save = Model;


			//positioning
			Model = glm::translate(Model, wallPos.at(i));
			Model = glm::scale(Model, glm::vec3(drawScale, drawScale, 4*drawScale));

			//das gleiche wie in der for schleife
			
			switch (i) {
				case 0:
					//links ?
					Model = glm::rotate(Model, 90.0f, glm::vec3(0, 1, 0));
					Model = glm::rotate(Model, 90.0f, glm::vec3(1, 0, 0));
					Model = glm::rotate(Model, 180.0f, glm::vec3(0, 1, 0));
					break;
				case 1:
					//unten
					Model = glm::rotate(Model, 90.0f, glm::vec3(1, 0, 0));
					Model = glm::rotate(Model, 90.0f, glm::vec3(0, 0, 1));
					Model = glm::rotate(Model, 90.0f, glm::vec3(1, 0, 0));
					break;
				case 2:
					//rechts ?
					Model = glm::rotate(Model, 90.0f, glm::vec3(0, 1, 0));
					Model = glm::rotate(Model, -90.0f, glm::vec3(1, 0, 0));
					Model = glm::rotate(Model, -180.0f, glm::vec3(0, 1, 0));
					break;
				case 3:
					//oben
					Model = glm::rotate(Model, 90.0f, glm::vec3(1, 0, 0));
					Model = glm::rotate(Model, 90.0f, glm::vec3(0, 0, 1));
					Model = glm::rotate(Model, -90.0f, glm::vec3(1, 0, 0));
					break;
			}
			

			sendMVP();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, wallTex);
			//glBindTexture(GL_TEXTURE_2D, simplePolyTex);

			// Set our "myTextureSampler" sampler to user Texture Unit 0
			glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);

			//glBindVertexArray(wallVertsArray);
			glBindVertexArray(simplePolyVertsArray);

			//glDrawArrays(GL_TRIANGLES, 0, wallVerts.size());
			glDrawArrays(GL_TRIANGLES, 0, simplePolyVerts.size());
			glBindVertexArray(0);

			Model = Save;

			// standartShader
			glUseProgram(programID);
		}
	}
}

void deployAnExplosion(glm::vec3 creationPosition)
{
		if (!simplePolyBuilt) {
			createSimplePolygon();
		}

		//create new explosion
		explosions.push_back(new explosion(creationPosition, explosionTTL));
		//add to vector

		testDrawExplosion = false;	
}


void drawExplosion() {

	for (auto bla = explosions.begin(); bla != explosions.end();) {

		//shader magic
		glUseProgram(programIDExpl);	//access to shader

			if (loc3 != -1)
			{
				wallTransparency = 0.2;
			}

		glUniform1f(loc3, wallTransparency);	//access to shader

		//save Model
		glm::mat4 Save = Model;

		Model = glm::translate(Model, (*bla)->getPosition());


		if (!gameLost) {
			(*bla)->calculateTimespan(speedModifier, 5.0f, 20.0f);
			Model = glm::scale(Model, glm::vec3(30.0 / (*bla)->getTTL(), 30.0 / (*bla)->getTTL(), 30.0 / (*bla)->getTTL()));
			Model = glm::rotate(Model, -90.0f, glm::vec3(1, 0, 0));
		}
		else {
			(*bla)->calculateTimespan(speedModifier, 2.0f, 0.0f);
			Model = glm::scale(Model, glm::vec3(30.0 / (*bla)->getTTL(), 30.0 / (*bla)->getTTL(), 30.0 / (*bla)->getTTL()));
			
			Model = glm::rotate(Model, -90.0f, glm::vec3(1, 0, 0));
			Model = glm::rotate(Model, 30.0f, glm::vec3(0, 1, 0));
			Model = glm::rotate(Model, -90.0f, glm::vec3(0, 0, 1));
		}
		sendMVP();

		//texturing
		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, explosionTex);

		// Set our "myTextureSampler" sampler to user Texture Unit 0
		glUniform1i(glGetUniformLocation(programIDExpl, "myTextureSampler"), 0);


		//final drawing !
		glBindVertexArray(simplePolyVertsArray);

		glDrawArrays(GL_TRIANGLES, 0, simplePolyVerts.size());
		glBindVertexArray(0);

		//reset Model
		Model = Save;

		//3. delete explosions at end of their lifespan		
		if((*bla)->canBeDeleted())
		{
			bla = explosions.erase(bla);
		}			
		else
		{
			++bla;	//deleting an element reduces the vector so this fixes a crash when trying to read past the last element
		}			
	}
}

void determineTimeForFuelCreation()
{
	if (createFuelMovingObject == false) {	//reduces time spamming in console...
		if (fuelTime > 0)
		{
			--fuelTime;
			//std::cout << fuelTime << std::endl;
		}
		else
		{
			createFuelMovingObject = true;
			//std::cout << "fuel ready to deploy" << std::endl;
		}
	}
}


/*
Die Loop fkt. welche die neuen positionen aus den Asteroiden liest und entsprechend an die Draw Methode weitergibt
Pr�ft auch, wie viele asteroiden im spiel sind und aktualisiert die anzahl
*/
void updateMovingObjects() {


	// first check that there are enough asteroids...
	while (movingObjects.size() < amountOfMovingObjectsInGame) {
		//running low on asteroids, generate some !
		
		movingObjectCreationCoordinateX = std::rand() % 11 + (-5);
		movingObjectCreationCoordinateY = std::rand() % 11 + (-5);
		//depthWhereMovingObjectsAreCreated = (std::rand() % 10 + 5);
		depthWhereMovingObjectsAreCreated = rand() % 400 + 200;
		//std::cout << "depth: " << depthWhereMovingObjectsAreCreated << std::endl;
				
		float saveScale = scaleFactor;
		//once a time create a real big asteroid
		float localRand = 1.0f / (float)((rand() % 11 + 1));

		if (localRand > 0.9f) {
			float localRand2 = 1.0f / (float)((rand() % 11 + 1));
			if (localRand2 > 0.9f) {
				scaleFactor = scaleFactor + 1.5f;
			}
		}
		else
			scaleFactor = scaleFactor * localRand;

		//create moving objects (asteroid or fuel)
		// construtor with rotation angles
		if (createFuelMovingObject)
		{

			//make sure fuel can be reached
			movingObjectCreationCoordinateX = std::rand() % 5 + (-2);
			movingObjectCreationCoordinateY = std::rand() % 5 + (-2);
			movingObjects.push_back(new fuel(glm::vec3(movingObjectCreationCoordinateX, movingObjectCreationCoordinateY, depthWhereMovingObjectsAreCreated), astDirection, astVelocity, saveScale, glm::vec3(0, 4, 0)));

			createFuelMovingObject = false;
			fuelTime += fuelTimeInterval;
		}			
		else
		{

			int randomAngleX = std::rand() % 7 + (-3);
			int randomAngleY = std::rand() % 7 + (-3);
			int randomAngleZ = std::rand() % 7 + (-3);
			glm::vec3 randomRotationSpeed = glm::vec3(randomAngleX, randomAngleY, randomAngleZ);

			int nullOneAxis = std::rand() % 3 + 1;
			
			//delete one rotation axis
			if(nullOneAxis == 1)
				if (randomRotationSpeed.y != 0 && randomRotationSpeed.z != 0)
					randomRotationSpeed.x = 0.0f;

			if (nullOneAxis == 2)
				if (randomRotationSpeed.x != 0 && randomRotationSpeed.z != 0)
					randomRotationSpeed.y = 0.0f;
			
			if (nullOneAxis == 3)
				if (randomRotationSpeed.x != 0 && randomRotationSpeed.y != 0)
				randomRotationSpeed.z = 0.0f;

			movingObjects.push_back(new asteroid(glm::vec3(movingObjectCreationCoordinateX, movingObjectCreationCoordinateY, depthWhereMovingObjectsAreCreated), astDirection, astVelocity / (scaleFactor * 1.5f), scaleFactor, glm::vec3(randomRotationSpeed.x * (2 / scaleFactor), randomRotationSpeed.y * (2 / scaleFactor), randomRotationSpeed.z * (2 / scaleFactor))));

		}
		scaleFactor = saveScale;
	}


	//check & refresh the positions of the asteroids and delete too far behind ones...
	// .. and draw them...
	for (std::vector<movingObject*>::iterator bla = movingObjects.begin(); bla != movingObjects.end();){
		(*bla)->calculateNewPosition(speedModifier);
		(*bla)->updateRotationAngles(speedModifier);

		//draw:
		glm::mat4 newPosition = glm::translate(Model, (*bla)->getPosition());
		newPosition = glm::rotate(newPosition, (*bla)->getRotationAngle().x, glm::vec3(1, 0, 0));
		newPosition = glm::rotate(newPosition, (*bla)->getRotationAngle().y, glm::vec3(0, 1, 0));
		newPosition = glm::rotate(newPosition, (*bla)->getRotationAngle().z, glm::vec3(0, 0, 1));

		if (dynamic_cast<fuel*>(*bla))
		{
			//std::cout << dynamic_cast<fuel*>(*bla) << std::endl;			
			drawFuel(newPosition);
		}			
		else if (dynamic_cast<asteroid*>(*bla))
		{
			//std::cout << dynamic_cast<asteroid*>(*bla) << std::endl;
			drawAsteroid(newPosition, (*bla)->getScale());
		}

		//cleanup if too far behind...
		if ((*bla)->getPosition().z < -10) {
			bla = movingObjects.erase(bla);
		}
		else
			++bla;	//deleting an element reduces the vector so this fixes a crash when trying to read past the last element
	}
}

void updateShip()
{
	//update ship position
	spaceship->setPosition(glm::vec3(moveShipX, moveShipY, 1));

	//rotationsausgleich
	if (compensateVert) {
		if ((shipNose > 1.0f))
			--shipNose * deltaTime * inputSpeedBias;
		if ((shipNose < -1.0f))
			++shipNose * deltaTime * inputSpeedBias;
	}

	if (compensateHZ){
		if ((shipFlaps > 10.0f) ) {
			shipFlaps -= 3.0f;
		}
		if ((shipFlaps > 5.0f) && shipFlaps <= 10.0f) {
			shipFlaps -= 1.0f;
		}

		if (shipFlaps > 0.0f && shipFlaps <= 5.0f)
		{
			shipFlaps -= 0.1f;
		}
			
		if ((shipFlaps < -10.0f))
		{
			shipFlaps += 3.0f;
		}

		if (shipFlaps < -5.0f && shipFlaps >= -10.0f)
		{
			shipFlaps += 1.0f;
		}
			
		if (shipFlaps < 0.0f && shipFlaps >= -5.0f)
		{
			shipFlaps += 0.1f;
		}			
	}
}

void checkCollisions() {

	bool collision = false;
	if (!gameLost) {
		for (auto bla = movingObjects.begin(); bla != movingObjects.end();) {

			collision = spaceship->detectCollision((*bla));

			if (collision) {

				if (dynamic_cast<asteroid*>(*bla)) {

					deployAnExplosion((*bla)->getPosition());
					//enable Shake, with time and amount
					cameraShake = true;
					cameraShakeTime = bigShakeTime;
					amountCameraShake = bigShakeAmount;

					//slowdown


					//process damage taken
					damageTaken += ((*bla)->getScale() / damageDecrease);

				}
				else if (dynamic_cast<fuel*>(*bla)) {
					fuelLeft += fuelChargeAmount;
					if (fuelLeft > 1.0f)
						fuelLeft = 1.0f;
				}

				//delete the object
				bla = movingObjects.erase(bla);

				collision = false;

			}
			else
				++bla;	//deleting an element reduces the vector so this fixes a crash when trying to read past the last element
		}
	}
}

//auto y = clamp(x, 0, 255);
template<typename T>
T clamp(T &val, T &min, T &max)
{
	return std::max(min, std::min(max, val));
}

void drawUI()
{
	if (billboardCreated == false)
	{
		createBillboard();
	}
	else
	{

		for (int i = 0; i < maxUIElements; ++i)
		{
			
			deltaTime = currentTime - lastTime;
			lastTime = currentTime;

			Projection = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, -5.0f, farPlane);

			billboardViewProjectionMatrix = Projection * View;

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			// Use our shader
			glUseProgram(billboardProgramID);

			// Bind our texture in Texture Unit 0
			glActiveTexture(GL_TEXTURE0);

			billboardLifeLevel = 0.0f;

			if (i == 0)	//difficulty
			{
				glBindTexture(GL_TEXTURE_2D, billboardDifficultyTexture);
				glUniform3f(billboardPosID, 0, -0.95, 0); // The billboard will be just above the cube
				glUniform2f(billboardSizeID, 1, 1);     // and 1m*12cm, because it matches its 256*32 resolution =)
				glUniform2f(billboardVertexOffsetID, billboardDifficultyScale.x/3, billboardDifficultyScale.y/2);
																										  
				//bar calculation values between 0.53 and 1.0
				if (gameDifficulty > 0.0f)
				{
					billboardLifeLevel = clamp((float)(0.48*gameDifficulty + 0.53), 0.0f, 1.0f);
				}
			}

			if (i == 1)	//Fuel
			{
				glBindTexture(GL_TEXTURE_2D, billboardFuelTexture);
				glUniform3f(billboardPosID, -0.7, 0.8, 0.0f); // The billboard will be just above the cube
				glUniform2f(billboardSizeID, 1, 1);     // and 1m*12cm, because it matches its 256*32 resolution =)
				glUniform2f(billboardVertexOffsetID, billboardFuelScale.x/2.5, billboardFuelScale.y/2.1);

				//bar calculation values between 0.53 and 1.0
				if (fuelLeft > 0.0f)
				{
					billboardLifeLevel = clamp((float)(0.48*fuelLeft + 0.53),0.0f,1.0f);
				}
			}

			if (i == 2)	//Damage
			{
				glBindTexture(GL_TEXTURE_2D, billboardDamageTexture);

				glUniform3f(billboardPosID, 0.7f, 0.8f, 0.0f); // The billboard will be just above the cube
				glUniform2f(billboardSizeID, 1, 1);     // and 1m*12cm, because it matches its 256*32 resolution =)
				glUniform2f(billboardVertexOffsetID, billboardDamageScale.x / 2.5, billboardDamageScale.y / 2.1);

																								  //bar calculation values between 0.53 and 1.0
				if (damageTaken > 0.0f)
				{
					billboardLifeLevel = clamp((float)(0.48*damageTaken + 0.53), 0.0f, 1.0f);
				}
			}

			glUniform1i(billboardTextureID, 0);
			glUniform4fv(billboardMyColorID, 1, billboardMyColor);
			glUniform3f(billboardCameraRight_worldspace_ID, View[0][0], View[1][0], View[2][0]);
			glUniform3f(billboardCameraUp_worldspace_ID, View[0][1], View[1][1], View[2][1]);
			glUniform1f(billboardLifeLevelID, billboardLifeLevel);
			glUniformMatrix4fv(billboardViewProjMatrixID, 1, GL_FALSE, &billboardViewProjectionMatrix[0][0]);

			// 1rst attribute buffer : vertices
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, billboard_vertex_buffer);
			glVertexAttribPointer(
				0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
			);

			// Draw the billboard !
			// This draws a triangle_strip which looks like a quad.
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

			glDisableVertexAttribArray(0);
		}

		//draw Onscreen Messages:

		if (canSteer == false)
		{
			//draw no fuel message
		}
	}
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}

void cameraHandling()
{
	float localRand = range * ((((float)rand()) / (float)RAND_MAX)) + MIN_RAND;

	//save Model
	glm::mat4 Save = Model;
	
	//near Cam
	if (cam2) {

		//view setup
		Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, farPlane);

		View = glm::lookAt(glm::vec3(0, 0, -5), // Camera is at (0,0,-5), in World Space
			glm::vec3(0, 0, 0),  // and looks at the origin
			glm::vec3(0, -1, 0)); // Head is up (set to 0,-1,0 to look upside-down)

		Model = glm::rotate(Model, 15.0f, glm::vec3(1, 0, 0));
		Model = glm::rotate(Model, 0.0f, glm::vec3(0, 1, 0));
		Model = glm::rotate(Model, 0.0f, glm::vec3(0, 0, 1));

		if (cameraShake)
			Model = glm::translate(Model, glm::vec3((moveShipX + (localRand * amountCameraShake * cameraShakeTime) / 100), moveShipY - 0.3 + ((localRand * amountCameraShake * cameraShakeTime) / 100), -3));
		else
			Model = glm::translate(Model, glm::vec3(moveShipX, moveShipY-0.3, -3));
		//Model = Save;
	}

	//internal Cockpit cam
	if (cam3) {
		//view setup
		Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, farPlane);

		View = glm::lookAt(glm::vec3(0, 0, 0.6), // Camera is at (0,0,-5), in World Space
			glm::vec3(0, 0, 10),  // and looks at the origin
			glm::vec3(0, -1, 0)); // Head is up (set to 0,-1,0 to look upside-down)
		
		Model = glm::rotate(Model, 0.0f, glm::vec3(0, 1, 0));
		Model = glm::rotate(Model, 0.0f, glm::vec3(0, 0, 1));

		static float cockpitCamNoseCompensate = 0.0f;
	
		//cockpit nose compensate
		Model = glm::rotate(Model, shipNose, glm::vec3(1, 0, 0));

		if (cameraShake)
			Model = glm::translate(Model, glm::vec3(moveShipX + ((localRand * amountCameraShake * cameraShakeTime) / 100), moveShipY + ((localRand * amountCameraShake * cameraShakeTime) / 100), -5.6));
		else
			Model = glm::translate(Model, glm::vec3(moveShipX, moveShipY, 0));
		//Model = Save;
	}

	//external Cam
	if (cam1) {
		//view setup
		Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, farPlane);

		View = glm::lookAt(glm::vec3(0, 0, -5), // Camera is at (0,0,-5), in World Space
			glm::vec3(0, 0, 0),  // and looks at the origin
			glm::vec3(0, -1, 0)); // Head is up (set to 0,-1,0 to look upside-down)
		
		//Shaaaaakkkkeeeeeeee !!!!
		if (cameraShake)
			//Model = glm::translate(Model, glm::vec3(localRand * amountCameraShake, localRand * amountCameraShake, 0));
			Model = glm::translate(Model, glm::vec3((localRand * amountCameraShake * cameraShakeTime) / 100, (localRand * amountCameraShake * cameraShakeTime) / 100, 0));
	}
}

void cameraShakeTimeout() {
	if ((cameraShakeTime > 0) && (cameraShake)) {
		--cameraShakeTime;
	}
	else {
		cameraShake = false;
	}
}

void gameLogic()
{
	//fuel diminishing...
	//first limit fuel to max 1.0f
	clamp(fuelLeft, 0.0f, 1.0f);

	fuelLeft -= fuelDininishFactor;// *deltaTime;

	//calculate Difficulty
	if ((int)currentTime > difficultyIncreaseTime)
	{
		if (actualDifficulty < maxDifficulty)
		{
			difficultyIncreaseTime += difficultyIncreaseTime;
			++actualDifficulty;
			//increase Speed Multiplyer and amount of asteroids spawned
			amountOfMovingObjectsInGame += 50;
		}
	}
	gameDifficulty = (float)actualDifficulty / (float)maxDifficulty;

	//game Lost calculation
	if (fuelLeft <= 0.0f)
		canSteer = false;

	if (damageTaken >= 0.99f)
	{
		canSteer = false;
		//gameLost animation
		//go to external cam
		cam1 = true;
		cam2 = false;
		cam3 = false;

		//shipFocus
		if (spaceship->getPosition().x < 0.1)
			//			setLeftPressed = true;
			moveShipX += ShipXAmountMoving * deltaTime;

		else if (spaceship->getPosition().x > 0.1)
			//			setRightPressed = true;
			moveShipX -= ShipXAmountMoving * deltaTime;

		if (spaceship->getPosition().y < 0.1)
			//			setUpPressed = true;
			moveShipY += ShipYAmountMoving * deltaTime;

		else if (spaceship->getPosition().y > 0.1f)
			//			setDownPressed = true;
			moveShipY -= ShipYAmountMoving * deltaTime;

		//animate to sideView
		resetCam = false;
		resetCamToOrg(true);

		//slowdown
		speedModifier = 0.2f;

		//createExplosion at Ship when final cameraposition is reached
		if (endXReached && endYReached && endZReached && !gameLost) {
			gameLost = true;
			deployAnExplosion(glm::vec3(0, 0, 0));

			//enable Shake, with time and amount
			cameraShake = true;

			cameraShakeTime = bigShakeTime+100;
			amountCameraShake = bigShakeAmount;
		}
	}
}

int main(void)
{
	std::cout << "starting..." << std::endl;
	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		exit(EXIT_FAILURE);
	}

	// Fehler werden auf stderr ausgegeben, s. o.
	glfwSetErrorCallback(error_callback);

	// Open a window and create its OpenGL context
	// glfwWindowHint vorher aufrufen, um erforderliche Resourcen festzulegen
	GLFWwindow* window = glfwCreateWindow(
											screenWidth, // Breite
											screenHeight,  // Hoehe
											gameTitle, // Ueberschrift
											NULL,  // windowed mode
											NULL); // shared windoe

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Make the window's context current (wird nicht automatisch gemacht)
    glfwMakeContextCurrent(window);

	// Initialize GLEW
	// GLEW erm�glicht Zugriff auf OpenGL-API > 1.1
	glewExperimental = true; // Needed for core profile

	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Auf Keyboard-Events reagieren
	glfwSetKeyCallback(window, key_callback);

	// Dark black background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("StandardShading.vertexshader", "StandardShading.fragmentshader");
	programIDanim = LoadShaders("StandardShading_anim.vertexshader", "StandardShading_anim.fragmentshader");
	programIDExpl = LoadShaders("StandardShading.vertexshader", "StandardShading_anim_EXPL.fragmentshader");
	billboardProgramID = LoadShaders("Billboard.vertexshader", "Billboard.fragmentshader");

	//acces to animated shader vars
	bool locCheck = (loc = glGetUniformLocation(programIDanim, "mvUV"));	//access to mvUV in the shader
	std::cout << "locCheck: " << locCheck << " ,loc: " << loc << std::endl;

	bool locCheck2 = (loc2 = glGetUniformLocation(programIDanim, "discardValue"));	//access to transparency value
	std::cout << "locCheck: " << locCheck2 << " ,loc2: " << loc2 << std::endl;	

	bool locCheck3 = (loc3 = glGetUniformLocation(programIDExpl, "discardValue"));	//access to transparency value
	std::cout << "locCheck: " << locCheck3 << " ,loc3: " << loc3 << std::endl;

	// Shader auch benutzen !
	glUseProgram(programID);

	//initialize rand
	srand(static_cast <unsigned> (time(0)));

	//set lastTime
	lastTime = glfwGetTime();

	// Eventloop
	while (!glfwWindowShouldClose(window))
	{

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //sonst wird Z-Buffer nicht benutzt	

		//culling needs rework of starwalls, are not set up correctly
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		//glFrontFace(GL_CW);

		//glDisable(GL_TEXTURE_2D);
		glEnable(GL_LIGHTING);
	
		// Model matrix : an identity matrix (model will be at the origin)
		Model = glm::mat4(1.0f);
		Model = glm::rotate(Model, angleX, glm::vec3(1, 0, 0));
		Model = glm::rotate(Model, angleY, glm::vec3(0, 1, 0));
		Model = glm::rotate(Model, angleZ, glm::vec3(0, 0, 1));

		glm::mat4 Save = Model;

		// Compute time difference between current and last frame
		currentTime = glfwGetTime();
		deltaTime = (float(currentTime - lastTime) * 100.0f);
		lastTime = currentTime;
		//measure fps
		if ((int)currentTime > lastSecond)
		{
			lastSecond = currentTime;
			fpsOut = fps;
			fps = 0;
			//std::cout << "fps: " << fpsOut << std::endl;
		}
		else
			++fps;

		
		gameLogic();
		cameraHandling();

		//input
		keyInput(window);

		//cam stuff
		if (resetCam)
		{
			resetCamToOrg(false);
		}

		//calculate fuel time
		determineTimeForFuelCreation();

		//asteroid update (inkl creating, positioning and drawing)
		updateMovingObjects();

		//ship from here
		//positioning and stuff inside drawShip		
		drawShip();
		updateShip();
		checkCollisions();
		drawExplosion();
		cameraShakeTimeout();

		//drawStarDingens
		drawWall();

		//environment
		drawEnvironment();

		//finalizing...
		drawUI();
		// Swap buffers
		glfwSwapBuffers(window);

		// Poll for and process events 
        glfwPollEvents();
	} 

	//cleanup
	
	//asteroid
	glDeleteBuffers(1, &asteroidVertexBuffer);
	glDeleteBuffers(1, &asteroidNormalBuffer);

	glDeleteBuffers(1, &asteroidUVBuffer);
	glDeleteTextures(1, &astTex);

	//fuel
	glDeleteBuffers(1, &fuelVertexBuffer);
	glDeleteBuffers(1, &fuelNormalBuffer);

	glDeleteBuffers(1, &fuelUVBuffer);
	glDeleteTextures(1, &fuelTex);

	//ship
	glDeleteBuffers(1, &fuelVertexBuffer);
	glDeleteBuffers(1, &fuelNormalBuffer);

	glDeleteBuffers(1, &fuelUVBuffer);
	glDeleteTextures(1, &fuelTex);

	//simplePoligon
	glDeleteBuffers(1, &simplePolyVertexBuffer);
	glDeleteBuffers(1, &simplePolyNormalBuffer);

	glDeleteBuffers(1, &simplePolyUVBuffer);
	glDeleteTextures(1, &simplePolyTex);

	//sphere
	glDeleteBuffers(1, &spaceEnvVertexBuffer);
	glDeleteBuffers(1, &spaceEnvNormalBuffer);

	glDeleteBuffers(1, &spaceEnvUVBuffer);
	glDeleteTextures(1, &spaceEnvTex);

	//billboards
	glDeleteBuffers(1, &billboard_vertex_buffer);
	
	glDeleteTextures(1, &billboardFuelTexture);
	glDeleteTextures(1, &billboardDifficultyTexture);
	glDeleteTextures(1, &billboardDamageTexture);

	//programs
	glDeleteProgram(programID);
	glDeleteProgram(programIDanim);
	glDeleteProgram(programIDExpl);
	glDeleteProgram(billboardProgramID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
	return 0;
}

