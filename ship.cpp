﻿#include <iostream>
#include "ship.h"


//ship::ship(glm::vec3 position, float scaleFactor): movingObject(position, glm::vec3(0,0,0), 0, scaleFactor, glm::vec3(0,0,0), glm::vec3(0,0,0))
ship::ship(glm::vec3 position, float scaleFactor) : movingObject(position, glm::vec3(0, 0, 0), 0, scaleFactor, glm::vec3(0, 0, 0))

{
	setCollisionRadiusXpos(1.5 * scaleFactor);
	setCollisionRadiusXneg(1.5 * scaleFactor);
	setCollisionRadiusYpos(1.5 * scaleFactor);
	setCollisionRadiusYpos(1.5 * scaleFactor);
	setCollisionRadiusZpos(0.8 * scaleFactor);
	setCollisionRadiusZneg(2.5 * scaleFactor);
}

void ship::updatePosition(glm::vec3 position)
{
	setPosition(position);
}


bool ship::detectCollision(movingObject* object)
{
	// bools indicating single axis collision
	bool collisionX = false;
	bool collisionY = false;
	bool collisionZ = false;
	
	// invert x and y position
	float invPosX = -(getPosition().x);
	float invPosY = -(getPosition().y);
	float PosZ = getPosition().z;

		
	// collision detection x axis
	if (invPosX < object->getPosition().x)
		collisionX = invPosX + getCollisionRadiusXpos() >= object->getPosition().x && object->getPosition().x - object->getCollisionRadiusXpos() <= invPosX;
	else
		collisionX = invPosX - getCollisionRadiusXneg() < object->getPosition().x && object->getPosition().x + object->getCollisionRadiusXneg() >= invPosX;

	// collision detection y axis
	if (invPosY < object->getPosition().y)
		collisionY = invPosY + getCollisionRadiusYpos() >= object->getPosition().y && object->getPosition().y - object->getCollisionRadiusYpos() <= invPosY;
	else
		collisionY = invPosY - getCollisionRadiusYneg() < object->getPosition().y && object->getPosition().y + object->getCollisionRadiusYneg() >= invPosY;

	// collision detection z axis

	if (PosZ < object->getPosition().z)
		collisionZ = PosZ + getCollisionRadiusZpos() >= object->getPosition().z && object->getPosition().z - object->getCollisionRadiusZpos() <= PosZ;
	else
		collisionZ = PosZ - getCollisionRadiusZneg() < object->getPosition().z && object->getPosition().z + object->getCollisionRadiusZneg() >= PosZ;

	// checks if collision on all axes detected
	if (collisionX && collisionY && collisionZ)
	{
		//std::cout << "Asteroid:" << object.getPosition().x << " " << object.getPosition().y << " " << object.getPosition().z << std::endl;
		//std::cout << "Schiff:" << getPosition().x << " " << getPosition().y << " " << getPosition().z << std::endl;
		//collisionReaction();
		return true;
	}
	else {
		return false;
	}
	
}

void ship::collisionReaction()
{
	std::cout << "Raumschiff mit Objekt kollidiert!" << std::endl;
}
