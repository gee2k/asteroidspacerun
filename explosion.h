#pragma once
#include <glm/glm.hpp>


class explosion
{
public:

	explosion(glm::vec3 position, int ttl);

	~explosion();

	bool canBeDeleted() const;
	void calculateTimespan(float slowDown, float decrease, float deleteMeThreshold);
	float getTTL() const;
	glm::vec3 getPosition() const;

private:
	glm::vec3 m_position;
	float m_ttl;
	bool m_deleteMe = false;


	
};
