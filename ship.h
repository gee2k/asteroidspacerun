﻿#pragma once

#include "movingObject.h"

class ship : public movingObject
{
public:
	ship(glm::vec3 position, float scaleFactor);
	
	void updatePosition(glm::vec3 position);

	bool detectCollision(movingObject* object) override;

	void collisionReaction() override;			
};
