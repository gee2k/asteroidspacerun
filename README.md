# Asteroid Space Run #

![2016-09-15-13_00_11-CG---Tutorial.png](https://bitbucket.org/repo/ELkoyL/images/3278657293-2016-09-15-13_00_11-CG---Tutorial.png)
*yeah, ingame grafics !*

### Index ###
* [Sprachen](#markdown-header-sprachen)
* [Beschreibung](#markdown-header-beschreibung)
* [Tastenbelegung](#markdown-header-tastenbelegung)
* [Spielinhalt](#markdown-header-spielinhalt)

### Sprachen ###

* C++
* OpenGL
* GLSL

### Beschreibung ###

Projekt wurde im Rahmen des Moduls Computergrafik auf Grundlage einer vorgegebener OpenGL Basis erstellt.
Das Spiel wurde als Teamarbeit angefertigt.

### Tastenbelegung ###

| Tasten | Belegung |
| ------ | -------- |
| **1,2,3** | verschiedene Kamera Perspektiven |
| **Pfeiltasten** | Bewegungsrichtungen des Raumschiffs |
| **Esc** | Spiel beenden. |

### Spielinhalt ###

Ziel ist es, stehts genug **Treibstoff** zu haben. Dieser kann durch einsammeln der **roten Kanister** aufgefüllt werden. Des weiteren sollte man **keinen** Schaden durch **Asterroiden** nehmen.

Die **Anzeigen** geben oben Mitten den aktuellen **Schwierigkeitsgrad** an, je höher, desto mehr Asteroiden befinden sich vor dem Spieler.
Die Linke gibt den aktuellen **Treibstoffstand**, die Rechte den **Schaden** des Raumschiffs aus.

Das Spiel ist verloren, wenn die Schadensanzeige 100% erreicht.
Dies kann durch zu viele Kollissionen oder durch das Ausgehen des Treibstoffs passieren, welcher die Steuerung deaktiviert und man unweigerlich in Asteroiden fliegen wird.